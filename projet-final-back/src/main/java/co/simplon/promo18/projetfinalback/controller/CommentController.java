package co.simplon.promo18.projetfinalback.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinalback.entities.Comment;
import co.simplon.promo18.projetfinalback.repository.CommentRepository;

@RestController
public class CommentController {
    @Autowired
    private CommentRepository repo;
    
    // @GetMapping("/api/comment")
    // public List<Comment> all(){
    //     return repo.findAll();
    // }
    @GetMapping("api/comment/{id}")
    public List <Comment> byIdArticle (@PathVariable int id){
        List<Comment> comments = repo.findByIdArticle(id);
        return comments;
    }

    @PostMapping("/api/comment/{id}")
    public Comment add (@RequestBody Comment comment, @PathVariable int id){
        if (comment.getDate() == null ){
            comment.setDate(LocalDate.now());
               
        }
        repo.save(comment,id);
        return comment;
    }

    @DeleteMapping("/api/comment/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteArticle(@PathVariable int id) {
         if(!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}

    

