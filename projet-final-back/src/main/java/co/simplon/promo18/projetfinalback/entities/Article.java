package co.simplon.promo18.projetfinalback.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Article {
    private Integer id;
    private String titre;
    private String ingredients;
    private String preparation;
    private String image;
    private LocalDate date;
    private int category; 
    private List<Comment> comments = new ArrayList<>();


    public Article(String titre, String ingredients, String preparation, String image, LocalDate date, int category) {
        this.titre = titre;
        this.ingredients = ingredients;
        this.preparation = preparation;
        this.image = image;
        this.date = date;
        this.category = category;
    }
    public List<Comment> getComments() {
        return comments;
    }
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    public Article() {
    }
    public Article(String titre, String ingredients, String preparation, String image, LocalDate date) {
        this.titre = titre;
        this.ingredients = ingredients;
        this.preparation = preparation;
        this.image = image;
        this.date = date;
    }
    public Article(Integer id, String titre, String ingredients, String preparation, String image, LocalDate date) {
        this.id = id;
        this.titre = titre;
        this.ingredients = ingredients;
        this.preparation = preparation;
        this.image = image;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }
    public String getIngredients() {
        return ingredients;
    }
    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }
    public String getPreparation() {
        return preparation;
    }
    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public void setComment(List<Comment> listComment) {
    }
    public int getCategory() {
        return category;
    }
    public void setCategory(int category) {
        this.category = category;
    }   
}
