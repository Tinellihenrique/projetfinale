package co.simplon.promo18.projetfinalback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetFinalBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetFinalBackApplication.class, args);
	}

}
