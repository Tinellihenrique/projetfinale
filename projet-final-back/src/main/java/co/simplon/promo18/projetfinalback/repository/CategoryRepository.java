package co.simplon.promo18.projetfinalback.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.projetfinalback.entities.Category;


@Repository
public class CategoryRepository {
   
    @Autowired
    private DataSource dataSource;
    

    public List<Category> findAll() {
        List<Category> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");

            // ele vai preparar e mostrar os pedidos da base de dados, vai fazer um 
            // laco e vai pegar cada produto qte acabar e sair do laco, preparestatement e resultset

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Category category = new Category(
                        rs.getInt("id"),
                        rs.getString("label"));
                list.add(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("database error");
        }
        return list;
    }

}
