package co.simplon.promo18.projetfinalback.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.io.File;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.projetfinalback.entities.Article;
import co.simplon.promo18.projetfinalback.repository.ArticleRepository;
import co.simplon.promo18.projetfinalback.repository.CommentRepository;

@RestController
public class ArticleController {
    @Autowired
    ArticleRepository repo;
    @Autowired
    CommentRepository commentRepo;

    @GetMapping("/api/article")
    public List<Article> all(){
        return repo.findAll();
    }

    @GetMapping("/api/article/category/{id}")
    public List<Article> oneCategory (@PathVariable int id){
        List<Article> articles = repo.findByIdCategory(id);
        return articles;
    }

    @GetMapping("/api/article/{id}") //
    public Article one(@PathVariable int id) {
        Article article = repo.findById(id);
        if (article == null) { 
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setComments(commentRepo.findByIdArticle(id));

        return article;
    }

    @DeleteMapping("/api/article/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable int id){
        if (!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    
    @PutMapping("/api/article/{id}")
    public Article update(@PathVariable int id,
     @RequestBody Article article) {
         if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }
        if(!repo.update(article)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }

    @PostMapping("/api/article")
    public Article add(@ModelAttribute Article post, 
                    @ModelAttribute MultipartFile upload)
                 {
        // post.setAuthor(user);
        post.setDate(LocalDate.now());
        post.setId(null);
        System.out.println(post.getCategory());

        // vai criar um nome exclusivo para o arquivo para evitar colisões de nomes (tipo 2 pessoas que fazem upload de um arquivo chat.jpg)
        // e fazemos questão de recuperar a extensão do ficheiro original para concatená-la a este número
        String filename = UUID.randomUUID() + "."+ FilenameUtils.getExtension(upload.getOriginalFilename());
        // aqui vai pegar a foto e trocar o nome 
        try {
            // O arquivo é transferido para o arquivo de consulta na pasta de upload, com seu novo nome
            upload.transferTo(new File(getUploadFolder(), filename));
            //Armazenamos o nome do arquivo no banco de dados para saber qual arquivo está vinculado a qual post
            post.setImage(filename);
            repo.save(post);
        } catch (IllegalStateException | IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
        }
        return post;
    }
 /**
     * Método que recuperará a pasta upload (que será a pasta static/uploads no projeto)
     * e criá-lo se esta pasta não existir.
     * Para recuperar a pasta de destino, use o classLoader.getResource(".") que retorna o
     * pasta onde se encontra o código executado do servidor.
     * Idealmente, em um contexto de produção, seria melhor ter um registro
     * upload separado do projeto, mas em um contexto de desenvolvimento, é mais portátil
     * de fazer assim.
     */
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/image"));
        if(!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
            
    }


}
