-- Active: 1648455659986@@127.0.0.1@3306@projet_finale

DROP DATABASE IF EXISTS projet_finale;

CREATE DATABASE projet_finale;

USE projet_finale;

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(64),
    last_name VARCHAR(64),
    email VARCHAR(64),
    password VARCHAR(64),
    role VARCHAR(64)
);

CREATE TABLE category(
    id INT AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(64)
);

CREATE TABLE article (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR (265),
    ingredients TEXT,
    preparation TEXT,
    image VARCHAR(128),
    date DATE,
    user_id INTEGER,
    category_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);
-- article tem relacao many to one com user 
-- entao aonde tem a * é aonde vai sezr colocado o FOREIGN key 


CREATE TABLE comment(
    id INT AUTO_INCREMENT PRIMARY KEY,
    comment VARCHAR(500),
    date DATE,
    article_id INTEGER,
    user_id INTEGER,
    FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
);

INSERT INTO category (label) VALUES
("Apéritifs"),
("Entrées"),
("Plats"),
("Desserts"),
("Boissons");

INSERT INTO article
(titre,ingredients,preparation,image,date,category_id) VALUES
("Pains au fromage brésiliens (Pão de Queijo)",
"250 g de tapioca, 200 ml de lait entier, 30 g de beurre, 125 g de parmesan râpé, 2 œufs",
"Préchauffez le four à 200°. Faites bouillir le lait avec le beurre et le sel. Dans le bol de votre robot pétrisseur muni du crochet, 
versez le tapioca et recouvrez-le avec le lait bouillant. Mélangez avec une cuillère en bois de manière à ce que le tapioca absorbe le liquide.
 Ajoutez les œufs et le parmesan et commencez le pétrissage pendant une 10aine de minutes.",
"PaoDeQueijo-2577183_1920.jpg",
"2021-10-11",1),

("Coxinha(croquette brésilienne à la viande hachée)",
"2 pommes de terre cuites vapeur et réduites en purée,48 cl de lait,
1 c. à soupe de beurre,300 g de farine,24 cl d'eau,2 cubes de bouillon de volaille",
"PRÉPARATION :50minCUISSON :10min,1.Préparez la farce. Pelez et émincez l'oignon et l'ail. 
2.Faites chauffer l'huile dans une casserole.3.Faites revenir l'oignon et l'ail jusqu'à ce que l'oignon soit translucide.",
"coxinha-gc18355c04_1920.jpg",
"2020-11-12",1),

("Truffes au chocolat (Brigadeiros)",
"1 boîte de lait concentré (397 g),4 cuillères à soupe de chocolat en poudre,1 cuillère à soupe de beurre doux, vermicelles au chocolat ",
"Dans une casserole, faire fondre le beurre, puis ajouter le lait concentré et le chocolat en poudre,Faire chauffer à feu moyen, en remuant sans arrêt avec une cuillère en bois, 
Laisser cuire sans cesser de remuer, jusqu'à ce que le fond de la casserole apparaîsse (environ 10 minutes),Mettre la pâte au réfrigérateur pour qu'elle refroidisse.   ",
"Brigadeiro.jpg",
"2019-09-11",2),

("Moqueca de camarão da Bahia (crevettes au lait de coco)",
"700 g de crevettes crues, Le jus de 2 citrons,2 oignons, 2 gousses d’ail, 20 cl de lait de coco, 3 cuillères à café d’huile de colza ou d’arachide",
"Décortiquez les crevettes et mettez-les dans un petit saladier. Arrosez-les de jus de citron et laissez mariner quelques minutes, 
Pelez les oignons et les gousses d’ail. Coupez les oignons en rondelles et hachez l’ail. Taillez les poivrons en lamelles. Pelez les tomates 
(ébouillantez-les au préalable quelques secondes pour vous faciliter la tâche) et épépinez-les avant de les tailler en rondelles pas trop fines.",
"moqueca.jpg",
"2017-12-05",3),

("Caïpirinha (cocktail cachaça ou vodka, citron vert ou fruit de la passion )",
"30 cl de cachaça ou de vodka, 2 citrons verts, 4 cuillères de sucre, 500 ml de glace pilée,  ",
"Coupez les citrons verts en quartiers à l’aide d’un couteau bien aiguisé et retirez les membranes centrales blanches. 
Coupez chaque quartier de citron en petits dés et disposez-les dans un récipient. Versez le sucre sur les dés de citron, écrasez le tout à l’aide du pilon,
Ajoutez la cachaça (ou la vodka), la glace pilée puis mélangez directement dans le récipient ou dans un shaker, si vous en possédez un.
Répartissez le cocktail dans des verres préalablement glacés au réfrigérateur. ",
"caipirinha.jpg",
"2018-01-12",4),

("Feijoada (cassoulet brésilien)",
"30 cl de cachaça ou de vodka, 2 citrons verts, 4 cuillères de sucre, 500 ml de glace pilée,  ",
"Rincez les haricots. Faites-les tremper dans un saladier d’eau froide toute une nuit au réfrigérateur. 
Rincez les viandes salées et séchées. Le lendemain, taillez les viandes et les saucisses en morceaux, en prenant soin d’ôter le gras.
Pelez les oignons et l’ail, puis coupez les oignons en rondelles et écrasez l’ail. Taillez la branche de céleri en tronçons de 5 cm de longueur. 
Placez les morceaux de viande et l’os de porc. Ajoutez les haricots égouttés, 3 oignons, l’ail, le céleri, les feuilles de laurier, le thym ou de coriandre. 
Salez et poivrez.",
"Feijoada .jpg",
"2018-02-12",5);


INSERT INTO user 
(name, last_name,email,password,role) VALUES
("user_name","user_lastname","user@test.com","1234","ROLE_USER"),
("admin_name","admin_firstname","admin@test.com","1234","ROLE_ADMIN"),
("Pedro","Silva","pedro.silva@hotmail.com","1234","ROLE_USER"),
("Maria","Souza","maria.souza@hotmail.com","1234","ROLE_USER");



INSERT INTO comment (comment,date) VALUES
("Très bonne recette, mes invités se sont régalés. À refaire.","2020-10-10"),
("Super bon et très facile, peut-être avec un peu moins de sel et un peu plus de crème ","2021-11-11"),
("Une véritable catastrophe ! Jamais eu un résultat aussi désastreux.","2020-10-22");

select * from comment inner join article on article.id=comment.article_id WHERE article.id=2;





   
