import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { ArticleService } from '../article.service';
import { Article } from '../entities';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.css']
})
export class UpdateArticleComponent implements OnInit {
  
  article?:Article;
  

  constructor(private artService: ArticleService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.artService.getById(params['id']))
    )
      .subscribe(data => this.article = data);

  }

  update() {
    if(this.article)
    this.artService.put(this.article).subscribe();
  }
}

