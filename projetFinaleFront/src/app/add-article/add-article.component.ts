import { Component, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  categories: Category[] = []
  article: Article = {
    id: 0,
    titre: "",
    ingredients: "",
    preparation: "",
    image: "",
    date: "",
    category: 1,
  };

  idCategory = 0
  selection: EventEmitter<number> = new EventEmitter()


  upload?: File;

  displayAddArticle: boolean = false;
  constructor(private artService: ArticleService, private categoryService:CategoryService,private router: Router) { }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => this.categories = data);

  }
  showAddArticleForm() {
    if (!this.displayAddArticle) {
      this.displayAddArticle = true;
    } else if (this.displayAddArticle) {
      this.displayAddArticle = false;;
    }
  }
  addArticle() {
    console.log(this.article);
    this.artService.add(this.article, this.upload!).subscribe((data) => {
      this.router.navigate(['/article', data.id]);
    });
  }
  changeFile(event: any) {
    if (event.target.files.length > 0) {
      this.upload = event.target.files[0]; //On récupère le fichier mis dans l'input et on le stock dans le component
    }

  }
  select() {
    this.selection.emit(this.idCategory)
  }
}

