import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Article } from './entities';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http:HttpClient ) { }
  getAll(){
    return this.http.get<Article[]>(environment.apiUrl+'/api/article/');
  }
  getById(id:number) {
    return this.http.get<Article>(environment.apiUrl+'/api/article/'+id);
  }
  // add(article:Article){
  //   return this.http.post<Article>(environment.apiUrl+'/api/article', article);
  // }
  delete(id:number) {
    return this.http.delete(environment.apiUrl+'/api/article/'+id);
  }

  put(article:Article){
    return this.http.put<Article>(environment.apiUrl+'/api/article/'+article.id, article);
  }
  getByIdCategory(id:number) {
    return this.http.get<Article[]>(environment.apiUrl+'/api/article/category/'+id);
  }

  add(article:Article, upload:File) {
    
    const form = new FormData();
    form.append('titre', article.titre);
    form.append('ingredients', article.ingredients);
    form.append('preparation', article.preparation);
    form.append('upload', upload);
    if(article.category){
    form.append('category', article.category.toString());
  }
    return this.http.post<Article>(environment.apiUrl+'/api/article', form);
  }
  // formdata é usado para enviar informaçoes 
}

