import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { ArticleService } from '../article.service';
import { Article, Comment } from '../entities';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {

  constructor(private service: ArticleService, private route: ActivatedRoute, private router:Router) { }
  article?: Article;
  comment?: Comment;

  updateList(comment: Comment) {
    //console.log(comment);
    this.article?.comments?.push(comment);

  }
  ngOnInit(): void {
    /**
    * En gros ici, on récupère l'id de la route et on s'en sert pour faire un appel
    * vers /api/student/{id} en lui donnant l'id qu'on a récupéré dans la route.
    * Histoire de faire la manière la plus "optimisée" on utilise ici le pipe et le
    * switchMap des observable, c'est assez complexe à expliquer et à comprendre,
    * mais en soit, ce petit bout de code, vous pouvez le réutiliser presque tel quel
    * pour toutes les pages où vous avez besoin de récupérer une donnée par son id
    */
    this.route.params.pipe(
      switchMap(params => this.service.getById(params['id']))
    )
      .subscribe(data => this.article = data);

  }
  delete(id:number) {
 
    this.service.delete(id).subscribe(()=>this.router.navigate(['/']));
  

  }
  update(id:number){
      this.router.navigate(['/admin/', id]);
  }
}
