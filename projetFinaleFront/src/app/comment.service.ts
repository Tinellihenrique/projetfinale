import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Comment } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http:HttpClient) { }
    add(comment:Comment, id:number){
    return this.http.post<Comment>(environment.apiUrl+'/api/comment/'+id,comment );
  }
}
