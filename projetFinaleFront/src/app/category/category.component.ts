import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../category.service';
import { Category } from '../entities';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  @Output()
  selection:EventEmitter<number> = new EventEmitter()
  categories:Category[] = []
  idCategory=0

  constructor(private categoryService:CategoryService, private router:Router) { }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => this.categories = data);

  }
  select(){
    this.selection.emit(this.idCategory)
  }
}
